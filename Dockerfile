# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

ARG BASE_IMAGE=registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v10.5.0rc4

FROM $BASE_IMAGE as build

ARG MEZOS_PROTO_DEFAULT
ENV MEZOS_PROTO_DEFAULT=${MEZOS_PROTO_DEFAULT:-"16"}
ARG MEZOS_PROTO_FALLBACK
ENV MEZOS_PROTO_FALLBACK=${MEZOS_PROTO_FALLBACK:-"15"}

RUN echo $MEZOS_PROTO_DEFAULT $MEZOS_PROTO_FALLBACK
USER tezos
WORKDIR /home/tezos
ENV OPAMYES=true

RUN mv src tezos-indexer-src && rm -fr .git
# RUN rm -fr .git

COPY --chown=tezos .git .git
# RUN sudo chown -R tezos .git

RUN git checkout .
RUN eval $(opam env) ; make ${MEZOS_PROTO_DEFAULT} && ln -f mezos.exe mezos${MEZOS_PROTO_DEFAULT}.exe
RUN eval $(opam env) ; make ${MEZOS_PROTO_FALLBACK} && ln -f mezos.exe mezos${MEZOS_PROTO_FALLBACK}.exe

#Multistage : build end image only with necesary binaries
FROM $BASE_IMAGE

WORKDIR /home/tezos
COPY --from=build --chmod=555 /home/tezos/mezos*.exe /home/tezos/run-mezos.bash ./

USER tezos
RUN ./mezos.exe --help
ENTRYPOINT ["./run-mezos.bash"]
