#!/bin/bash

if [[ "$NODE_URL" != "" && "$PROTO" == "" ]]
then
PROTOCOL=$(curl -s ${NODE_URL}/chains/main/blocks/head|jq .protocol| tr -d '"')
case $PROTOCOL in
    "PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg" ) PROTO=14 ;;
    "PtLimaPtLMwfNinJi9rCfDPWea8dFgTZ1MeJ9f1m2SRic6ayiwW" ) PROTO=15 ;;
    "PtMumbaiP9THCizHVy23Zu3xCjTpNLWirfXY3kitfyd2nkvTbCj" ) PROTO=16 ;;
esac
fi

RUN_MEZOS () {
    (./mezos${PROTO}.exe "$@" || ./mezos$((PROTO+1)).exe "$@")
}

REMOVE_IP () {
    sed -e 's/\(X-Forwarded-For|X-Real-Ip\) [^)]*)//g'
}

if [[ "$DELETE_IP_INFORMATION" != "false" ]]
then
    (RUN_MEZOS "$@" | REMOVE_IP) 2> >(REMOVE_IP >&2)
else
    RUN_MEZOS "$@"
fi
