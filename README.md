# Mezos — Middleware Tezos for Mobile Devices and more

**Warning:** This README is often outdated.

## Description

Mezos provides a middleware for mobile devices.
Technically, it connects to a PostGres database filled by
[`tezos-indexer`](https://gitlab.com/nomadic-labs/tezos-indexer) and
provides a set of RPC for mobile devices to use.
It also connects directly to the tezos-node for some wallet operations;
and that's the reason why you can't run the same Mezos code for all Tezos networks.

## Run from Docker image
```bash
docker run -it --env PROTO:12 registry.gitlab.com/nomadic-labs/mezos/mezos:v2.2.6 run --chain-db=postgresql://${PG_SERVER_HOSTNAME}:${PG_SERVER_PORT}/${DB_NAME} --tezos-url=http://${OCTEZ_TEZOS_NODE_HOSTNAME}:${OCTEZ_TEZOS_NODE_PORT} --mezos-url=http://${MEZOS_IP}:${MEZOS_PORT}
```

## Build from source code

Get the source code:
```bash
git clone https://gitlab.com/nomadic-labs/mezos
cd mezos
# Get the source dependencies
make tezos-indexer
make tezos -f tezos-indexer/tezos.Makefile
# Get the opam dependencies
(cd tezos-indexer && make install-opam-deps)
# if you didn't have mpp already installed, run this:
(cd tezos-indexer && ${MAKE} protos)
```

Build for ithaca:
```
make 12
```

And run it:
```
./mezos.exe run --chain-db=postgresql://${PG_SERVER_HOSTNAME}:${PG_SERVER_PORT}/${DB_NAME} --tezos-url=http://${OCTEZ_TEZOS_NODE_HOSTNAME}:${OCTEZ_TEZOS_NODE_PORT} --mezos-url=http://${MEZOS_IP}:${MEZOS_PORT}
```

Don't hesitate to consult
```
./mezos.exe --help
```
