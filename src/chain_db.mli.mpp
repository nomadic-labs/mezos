(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_protocol_014_PtKathma
open Protocol.Alpha_context
open Mezos_utils

(* al store_blk_full :
  *  ?chain:Block_services.chain ->
  *  ?block:Block_services.block ->
  *  #RPC_context.simple ->
  *  (module Caqti_lwt.CONNECTION) -> unit tzresult Lwt.t *)

type tx_full = {
  op_hash : Operation_hash.t ;
  id : int ;
  blk_hash : Block_hash.t ;
  level : int ;
  timestamp : Time.Protocol.t ;
  src : Contract.t ;
  src_mgr : Tezos_crypto.Signature.V1.public_key_hash option ;
  dst : Contract.t ;
  dst_mgr : Tezos_crypto.Signature.V1.public_key_hash option ;
  fee : Tez.t option ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

val tx_full_encoding : tx_full Data_encoding.t

module IntMap : Map.S with type key := int

val find_txs_involving_k :
  ((module Caqti_lwt.CONNECTION),
   [< Caqti_error.t
    > `Decode_rejected `Encode_failed `Encode_rejected
        `Request_failed `Request_rejected `Response_failed
        `Response_rejected ])
    Caqti_lwt.Pool.t ->
  Contract.t ->
  tx_full IntMap.t Operation_hash.Map.t Lwt.t


(* val bootstrap_chain :
 *   ?blocks_per_sql_tx:int32 ->
 *   ?from:int32 -> ?up_to:int32 ->
 *   first_alpha_level:int32 ->
 *   #full ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   int32 tzresult Lwt.t *)
(** [bootstrap_chain ?blocks_per_sql_tx ?from ?up_to cctxt db]
    download and store blocks from [cctxt] in [db]. [blocks_per_sql_tx]
    governs how many blocks will be downloaded before commiting them in
    SQLite. [from] is the starting point for downloading the chain
    (default [2l]). [up_to] is a target block where to stop downloading
    blocks. *)

(* val bootstrap_delegate :
 *   #full ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   Tezos_crypto.Signature.V1.Public_key_hash.t ->
 *   unit tzresult Lwt.t
 * (\** [bootstrap_delegate cctxt db delegate] downloads and stores
 *     delegate info for [delegate] from [cctxt] in [db]. *\) *)

(* val discover_initial_ks :
 *   #full ->
 *   Block_services.chain * Block_services.block ->
 *   (module Caqti_lwt.CONNECTION) ->
 *   unit tzresult Lwt.t *)
(** [discover_initial_ks cctxt blkid db] discovers all contracts
    present on the chain at [blkid] and write in [db]. *)

val history :
  ?display:bool ->
  ((module Caqti_lwt.CONNECTION),
   [< Caqti_error.t
    > `Decode_rejected `Encode_failed `Encode_rejected
        `Request_failed `Request_rejected `Response_failed
        `Response_rejected ])
    Caqti_lwt.Pool.t ->
  Tezos_protocol_014_PtKathma.Protocol.Alpha_context.Contract.t list ->
  tx_full IntMap.t Operation_hash.Map.t list tzresult Lwt.t
(** [history ?display db_pool ks] returns the lists of transactions
    found in database (via [db_pool]), corresponding to [k]. *)

(* val store_snapshot_levels :
 *   (module Caqti_lwt.CONNECTION) -> (int * int32) list -> unit Lwt.t *)

(* val snapshot_levels :
 *   (module Caqti_lwt.CONNECTION) -> int32 Cycle.Map.t Lwt.t *)

(** Balance updates *)

type balance_full = {
  level: int32 ;
  cycle: Cycle.t ;
  cycle_position: int32 ;
  op: (Operation_hash.t * int) option ;
  cat : Delegate.balance ;
  diff : Tez.t ;
}

val balance_full : balance_full Caqti_type.t
val select_balance_full :
  (Contract.t * int32 * int32,
   balance_full,
   Caqti_mult.zero_or_more) Caqti_request.t

(** Contracts. *)

module Contract_table : sig
  type t = {
    (* k: Contract.t ; *)
    blk_hash : Block_hash.t ;
    manager: Tezos_crypto.Signature.V1.Public_key_hash.t option ;
    delegate: Tezos_crypto.Signature.V1.Public_key_hash.t option ;
    spendable: bool option;
    delegatable: bool option;
    (* credit: Tez.t option ;
     * preorigination: Contract.t option ; *)
    script: Script.t option ;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  (* val select :
   *   (unit, t, Caqti_mult.zero_or_more) Caqti_request.t *)
  val select_by_k :
    (Contract.t, t, Caqti_mult.zero_or_one) Caqti_request.t

  val exists :
    (Contract.t, bool, Caqti_mult.zero_or_one) Caqti_request.t
end


module Contract_table2 : sig
  type t = {
      (* k: Contract.t ; *)
      blk_hash : Block_hash.t ;
      manager: Tezos_crypto.Signature.V1.Public_key_hash.t option ;
      delegate: Tezos_crypto.Signature.V1.Public_key_hash.t option ;
      spendable: bool option;
      delegatable: bool option;
      script: Script.t option;
      storage: Script.lazy_expr option ;
    }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding


  (* val select_by_mgr :
   *   (public_key_hash, t, Caqti_mult.zero_or_more) Caqti_request.t *)
  val select_by_k :
    (Contract.t, t, Caqti_mult.zero_or_one) Caqti_request.t
end

module Contract2 : sig
  type t = {
    k: Contract.t ;
    balance: int64;
    operation_hash: Operation_hash.t option;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  val select :
    (Tezos_crypto.Signature.V1.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t
end

module Contract3 : sig
  type t = {
    k: Contract.t;
    balance: int64;
    operation_hash: Operation_hash.t option;
    delegate: Tezos_crypto.Signature.V1.Public_key_hash.t option;
    storage : Script.lazy_expr option;
  }

  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding

  val select :
    (Tezos_crypto.Signature.V1.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t
end

module Multisig : sig
  val select_by_k_pkh :
    (Contract.t * Tezos_crypto.Signature.V1.Public_key_hash.t, Contract.t, Caqti_mult.zero_or_more) Caqti_request.t
end

module Identical_contracts : sig
  val select_by_k :
    (Contract.t, Contract.t, Caqti_mult.zero_or_more) Caqti_request.t
end


(* (\** Delegate DB. If you need delegate info cycle per cycle. *\)
 *
 * val select_delegate :
 *   (public_key_hash,
 *    Tezos_sql.delegate_info,
 *    Caqti_mult.zero_or_more) Caqti_request.t *)



val nb_alpha_operations :
  (module Caqti_lwt.CONNECTION) -> Operation_hash.t -> int Lwt.t


module Manager : sig
  type t = {
      manager: Tezos_crypto.Signature.V1.Public_key_hash.t option ;
    }
  val sql_encoding : t Caqti_type.t
  val json_encoding : t Data_encoding.encoding
  val select_by_k : (Contract.t, t, Caqti_mult.zero_or_one) Caqti_request.t

end

module Sop : sig
  (* type t = {
   *     src: Tezos_crypto.Signature.V1.Public_key_hash.t;
   *     src_pk: Tezos_crypto.Signature.V1.Public_key.t;
   *     binary: string;
   *     signature: Signature.t;
   *   }
   * val sql_encoding : t Caqti_type.t
   * val json_encoding : t Data_encoding.encoding *)
  val store :
    ((Tezos_crypto.Signature.V1.public_key_hash * Tezos_crypto.Signature.V1.public_key * string * Tezos_crypto.Signature.V1.t),
     unit, Caqti_mult.zero) Caqti_request.t

end

module Operations : sig

    type t = {
      (* For all operations *)
      op_type : string;
      op_id : int64;
      op_level : int64;
      op_timestamp : Time.Protocol.t;
      op_block : Block_hash.t ;
      op_hash : Operation_hash.t;

      (* For the 4 kinds of operations *)
      op_source : string option;
      op_fee : Tez.t option;
      op_counter : Z.t option;
      op_gas_limit : Z.t option;
      op_storage_limit : Z.t option;
      op_op_id : int32 option;

      (* internal operations *)
      op_internal: int32;         (* 0 means non internal *)
      op_nonce : int32 option;  (* None means non internal *)

      (* reveal *)
      op_public_key  : Tezos_crypto.Signature.V1.Public_key.t option;

      (* transaction *)
      op_amount : int64 option;
      op_destination : string option;
      op_parameters : Script.lazy_expr option ;
      op_entrypoint : string option;

      (* origination *)
      op_contract_address : Contract.t option;

      (* delegation *)
      op_delegate : Tezos_crypto.Signature.V1.Public_key_hash.t option;
      }

    val json_encoding : t Data_encoding.encoding

end

module Origination_table : sig
  type t = {
    op: Operation_hash.t ;
    op_id: int ;
    src: Contract.t ;
    k: Contract.t ;
  }

  val select_by_source :
    (Tezos_crypto.Signature.V1.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t

end

module Mempool_operations : sig
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  type t = {
    branch: Block_hash.t;
    ophash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Tezos_crypto.Signature.V1.public_key_hash option;
    destination: Tezos_raw_protocol_014_PtKathma.Alpha_context.Contract.t option;
    seen: float;
    json_op: string Lazy.t;
  }

  type select = {
    branch: Block_hash.t;
    ophash: Operation_hash.t;
    first_seen_level: int32;
    first_seen_timestamp: int32;
    last_seen_level: int32;
    last_seen_timestamp: int32;
    status: status;
    id: int64;
    operation_kind: string;
    source: Tezos_crypto.Signature.V1.public_key_hash option;
    destination: Tezos_raw_protocol_014_PtKathma.Alpha_context.Contract.t option;
    json_op: string;
    block: Block_hash.t option;
  }

  val string_of_status : status -> string
  val status_of_string : string -> status option
  val sql_encoding : t Caqti_type.t
  val insert : (t, unit, [ `One | `Zero ]) Caqti_request.t
  val select_by_ophash : (Operation_hash.t, select, [ `Many | `One | `Zero ]) Caqti_request.t
  val select_by_pkh : (Tezos_crypto.Signature.V1.Public_key_hash.t, select, [ `Many | `One | `Zero ]) Caqti_request.t

end

module Tokens : sig

  module Contracts : sig

    type kind = FA12 | FA2

    type t = {
      k: Contract.t;
      kind: kind;
    }

    val kind_json_encoding : kind Data_encoding.t

    val json_encoding : t Data_encoding.t
    val sql_encoding : t Caqti_type.t

    val select : (int64 * int64 * kind list, t, Caqti_mult.zero_or_more) Caqti_request.t
    val exists : (Contract.t, kind, Caqti_mult.zero_or_one) Caqti_request.t

    val find_accounts :
      (Contract.t list * int64 * int64 * kind list, t, Caqti_mult.zero_or_more) Caqti_request.t

  end

  module Operations : sig

    type action =
        Transfer | Approve | Get_balance | Get_allowance | Get_total_supply

    type t = {
      (* For all actions *)
      op_type : action;
      op_id : int64;
      op_level : int64;
      op_timestamp : Time.Protocol.t;
      op_block : Block_hash.t ;
      op_hash : Operation_hash.t;
      op_caller : Contract.t;
      op_tz_amount : Tez.t option;

      (* Options *)
      op_fee : Tez.t option;
      op_counter : Z.t option;
      op_gas_limit : Z.t option;
      op_storage_limit : Z.t option;
      op_op_id : int32 option;

      (* Tokens specifics *)
      op_action_source : Contract.t option; (* transfer, approve, getBalance, getAllowance *)
      op_action_destination : Contract.t option; (* transfer, getAllowance *)
      op_action_amount : Z.t option; (* transfer, approve *)
      op_action_callback : Contract.t option; (* get* *)

    }

    val json_encoding : t Data_encoding.t
    val sql_encoding : t Caqti_type.t

(**# let _ =
List.iter (fun (params_types, params) ->
List.iter (fun l ->
      let fn =
        List.fold_left
          (fun r e -> Printf.sprintf "%s_%s" r e)
          ""
          l
      in
      Printf.printf "
  val select%s_by_pkh%s : (((Contract.t * Contract.t %s) * (int64 * int32 option)), t, Caqti_mult.zero_or_more) Caqti_request.t
"
fn params params_types
)
    [
      [ "all_operations" ]
    ; [ "transfer"; "approve" ]
    ; [ "transfer" ]
    ; [ "approve" ]
    ]
)
[
"", "";
" * Time.Protocol.t", "_downto";
" * Time.Protocol.t", "_from";
" * Time.Protocol.t * Time.Protocol.t", "_from_downto";
]

   #**)
  end

end


module Multikind_operations : sig

  type kind = Transaction | Delegation | Reveal | Origination

  type status = Applied | Refused | Backtracked | Failed | Skipped

  type token_kind = FA12 | FA2 | Tez

  type operation_data = {
    amount: Tez.t option;
    token_amount: Z.t option;
    token: token_kind option;
    token_id: Z.t option;
    internal_op_id: int option;
    destination: Contract.t option;
    contract: Contract.t option;
    parameters: Script.lazy_expr option;
    entrypoint: string option;
    storage_size: Z.t option;
    paid_storage_size_diff: Z.t option;
    public_key: Tezos_crypto.Signature.V1.Public_key.t option;
    delegate: Tezos_crypto.Signature.V1.Public_key_hash.t option;
  }

  type t = {
    hash: Operation_hash.t;
    id: int64;
    block_hash: Block_hash.t option;
    op_timestamp: Time.Protocol.t;
    level: int64;
    internal: int32 option;
    kind: kind;
    src: Contract.t;
    status: status;
    fee: Tez.t option;
    data: operation_data option;
    counter: Z.t;
    gas_limit: Z.t;
    storage_limit: Z.t;
  }

  val kind_json_encoding : kind Data_encoding.t
  val kind_sql_encoding : kind Caqti_type.t

  val status_json_encoding : status Data_encoding.t
  val status_sql_encoding : status Caqti_type.t

  val operation_data_json_encoding : operation_data Data_encoding.t
  val operation_data_sql_encoding : operation_data Caqti_type.t

  val json_encoding : t Data_encoding.t
  val sql_encoding : t Caqti_type.t

  val select_by_addresses :
    ((Contract.t list * kind list * int64 * int64),
     t,
     Caqti_mult.zero_or_more)
      Caqti_request.t

end


(* __DB_QUERY_SIG__ *)
