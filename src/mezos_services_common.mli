(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* open Alpha_environment *)
open Tezos_protocol_016_PtMumbai.Protocol
open Protocol
open Alpha_context
open Apply_results
open Protocol_client_context

module Environment = Tezos_protocol_environment_016_PtMumbai
open Environment

val analytics :
  ([ `GET ],
   unit,
   unit,
   unit,
   unit,
   Mezos_analytics.full_metrics) RPC_service.service

val history :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.tx_full list list) RPC_service.service

val contracts :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Contract.t list list) RPC_service.service

val contracts2 :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Chain_db.Contract2.t list list) RPC_service.service

val contracts3 :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Chain_db.Contract3.t list list) RPC_service.service


val contract_info :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Contract_table.t option list) RPC_service.service

val contract_info2 :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Contract_table2.t list) RPC_service.service


val create_sop : (* création d'instance d'opération *)
  ([ `POST ],
   unit,
   unit,
   unit,
   (Tezos_crypto.Signature.V1.public_key_hash * Signature.public_key * string * Signature.t),
   unit) RPC_service.service


val missing_blocks :
  ([ `GET ],
   unit,
   unit,
   unit,
   unit,
   int32 list) RPC_service.service

val manager :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Manager.t option list) RPC_service.service


type operations_query = {
    destination : Contract.t option;
    types : [ `Reveal | `Transaction | `Origination | `Delegation ] list;
    from: int64 option;
    downto_: int64 option;
    limit: int32 option;
    lastid: int64 option;
  }

val operations :
  ([ `GET ],
   unit,
   unit * Contract.t,
   operations_query,
   unit,
   Chain_db.Operations.t list) RPC_service.service


val mempool_ops_pkh :
  ([ `GET ],
   unit,
   unit * Tezos_crypto.Signature.V1.Public_key_hash.t,
   unit,
   unit,
   Chain_db.Mempool_operations.select list) RPC_service.service

val mempool_ops_ophash :
  ([ `GET ],
   unit,
   unit * Operation_hash.t,
   unit,
   unit,
   Chain_db.Mempool_operations.select list) RPC_service.service

module Accounts : sig

  type balances_param = {
    accounts : Contract.t list;
    kt : Contract.t option;
    tokenid : Z.t option;
  }

  type amount =
    | Tez of Tez.t
    | Z of Z.t

  val multiple_balances :
    ([ `GET ],
     unit,
     unit,
     balances_param,
     unit,
     (Contract.t * amount) list) RPC_service.service

  val exists :
    ([ `GET ],
     unit,
     unit * Contract.t,
     unit,
     unit,
     bool) RPC_service.service

end

module Tokens : sig

  val viewer :
    ([ `GET ],
     unit,
     unit,
     unit,
     unit,
     Contract.t option) RPC_service.service

  type filter = {
    kinds : [ `FA1_2 | `FA2 ] list;
    accounts : Contract.t list;
    limit: int64 option;
    index: int64 option;
  }

  val contracts :
    ([ `GET ],
     unit,
     unit,
     filter,
     unit,
     Chain_db.Tokens.Contracts.t list) RPC_service.service

  val exists :
    ([ `GET ],
     unit,
     unit * Contract.t,
     unit,
     unit,
     Chain_db.Tokens.Contracts.kind option) RPC_service.service

  val find_tokens_for_single_account :
    ([ `GET ],
     unit,
     unit * Contract.t,
     unit,
     unit,
     Chain_db.Tokens.Contracts.t list) RPC_service.t

  val balance :
    ([ `GET ],
     unit,
     (unit * Contract.t) * Contract.t,
     unit,
     unit,
     Z.t) RPC_service.service


  type operations_query = {
    types : [ `Transfer | `Approve | `All_operations ] list;
    from: int64 option;
    downto_: int64 option;
    limit: int32 option;
    lastid: int64 option;
  }

  (* FIXME: Unused for now, needs to modify requests to select every tokens
     operations *)
  val account_operations :
    ([ `GET ],
     unit,
     unit * Contract.t,
     operations_query,
     unit,
     Chain_db.Tokens.Operations.t list) RPC_service.service

  val account_token_operations :
    ([ `GET ],
     unit,
     (unit * Contract.t) * Contract.t,
     operations_query,
     unit,
     Chain_db.Tokens.Operations.t list) RPC_service.service

end

val version :
    ([ `GET ],
     unit,
     unit,
     unit,
     unit,
     Mezos_monitor.Version.t) RPC_service.service

val monitor_blocks :
    ([ `GET ],
     unit,
     unit,
     unit,
     unit,
     Mezos_monitor.Monitor.Blocks.t) RPC_service.service

val monitor_mempool :
    ([ `GET ],
     unit,
     unit,
     unit,
     unit,
     Mezos_monitor.Monitor.Mempool.t) RPC_service.service

val status:
    ([ `GET ],
     unit,
     unit,
     unit,
     unit,
     unit) RPC_service.service

module Multikind_operations : sig

  type operations_query = {
    types : [ `Reveal | `Transaction | `Origination | `Delegation ] list;
    limit: int64 option;
    index: int64 option;
  }

  val multikind_operations :
    ([ `GET ],
     unit,
     unit * Contract.t,
     operations_query,
     unit,
     Chain_db.Multikind_operations.t list) RPC_service.service

end

val multisig :
  ([ `GET ],
   unit,
   unit,
   Contract.t option * Tezos_crypto.Signature.V1.Public_key_hash.t list,
   unit,
   (Tezos_crypto.Signature.V1.Public_key_hash.t * Contract.t list) list) RPC_service.service

val identical_contracts :
  ([ `GET ],
   unit,
   unit,
   Contract.t option,
   unit,
   Contract.t list) RPC_service.service

(* __RPC_SERVICE_SIG__ *)
