(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Contract_utils = Tezos_indexer_016_PtMumbai.Contract_utils

let ( >>= ) = Lwt.bind
let return_some e = Lwt.return_ok (Some e)
let return_none = Lwt.return_ok None

let translate_fa12_action operation data parameters =
  let open Chain_db.Multikind_operations in
  match Contract_utils.FA12.action_of_data
          data.Chain_db.Multikind_operations.entrypoint parameters with
  | Ok (Transfer (source, dest, amount)) -> {
      operation with
      src = source;
      data = Some
          { data with token_amount = Some amount;
                      destination = Some dest;
          };
    }
  | _ -> operation

let translate_fa2_transfer operation data txs =
  let open Chain_db.Multikind_operations in
  List.fold_left (fun (txs, i) (source, dests) ->
      List.fold_left
        (fun (txs, i) (dest, token_id, amount) -> {
             operation with
             src = source;
             data = Some
                 { data with token_amount = Some amount;
                             destination = Some dest;
                             token_id = Some token_id;
                             internal_op_id = Some i;
                 };
           } :: txs, i + 1)
        (txs, i)
        dests)
    ([], 0)
    txs

let translate_fa2_action operation data parameters =
  let open Chain_db.Multikind_operations in
  match Contract_utils.FA2.action_of_data data.entrypoint parameters with
  | Ok (Transfer transfers) ->
    translate_fa2_transfer operation data transfers |> fst
  | _ ->
    [ operation ]

let translate_token_action operation =
  let open Chain_db.Multikind_operations in
  match operation.block_hash, operation.data with
  | None, Some ({ token = Some FA12;
                  parameters = Some parameters} as data) ->
    [ translate_fa12_action operation data parameters ]
  | None, Some ({ token = Some FA2;
                  parameters = Some parameters} as data) ->
    translate_fa2_action operation data parameters
  | _ ->
    [ operation ]

let in_mempool operations =
  let open Chain_db.Multikind_operations in
  let check operation =
    match operation.block_hash, operation.data with
    | None, Some ({ token = Some (FA12 | FA2) }) -> true
    | _ -> false
  in
  List.exists check operations

let handle_tokens_in_mempool operations =
  List.map translate_token_action operations |> List.flatten

let check_contract cctxt contract =
  Contract_utils.Tokens.check_contract
    cctxt ~chain:cctxt#chain ~block:cctxt#block ~contract () >>= function
  | Ok Contract_utils.Tokens.FA2 -> return_some Chain_db.Tokens.Contracts.FA2
  | Ok Contract_utils.Tokens.FA12 -> return_some Chain_db.Tokens.Contracts.FA12
  | Error _ -> return_none
