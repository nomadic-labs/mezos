(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>           *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner
open Cmdliner_helpers

let version = "4.0.0"

type t = {
  more_logs: bool;
  debug: bool;
  tezos_url: Uri.t option;
  chain_db: Uri.t;
  mezos_url: Uri.t option;
  tezos_client_dir: string;
  timeout: string;
  drifting_warning: int;
  last_block_warning: int;
}

let default_remote_host = "localhost"
let default_tezos_rpc_port = 8732
let default_tezos_mainnet_rpc_port = 18732
let default_mezos_rpc_port = 8776

let default_tezos_mainnet_url =
  Uri.make
    ~scheme:"http"
    ~host:default_remote_host
    ~port:default_tezos_mainnet_rpc_port
    ()
let default_tezos_url =
  Uri.make
    ~scheme:"http"
    ~host:default_remote_host
    ~port:default_tezos_rpc_port
    ()
let default_mezos_url =
  Uri.make
    ~scheme:"http"
    ~host:default_remote_host
    ~port:default_mezos_rpc_port
    ()
let default_chain_url =
  Uri.make
    ~scheme:"postgresql"
    ~host:"localhost"
    ~path:"mainnet"
    ()

let default = {
  more_logs = false;
  debug = false;
  tezos_url = Some default_tezos_url;
  mezos_url = Some default_mezos_url;
  chain_db = default_chain_url;
  tezos_client_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client";
  timeout = "60s";
  drifting_warning = 2; (* 2 blocks of difference *)
  last_block_warning = 180; (* warning if the last block received by the
                                    node was more than 5 minutes ago *)
}

let build
    more_logs debug tezos_url mezos_url chain_db tezos_client_dir timeout
    drifting_warning last_block_warning _token_viewer () =
{
  more_logs;
  debug;
  tezos_url;
  chain_db;
  mezos_url;
  tezos_client_dir;
  timeout;
  drifting_warning;
  last_block_warning;
}
