module Hashtbl = Stdlib.Hashtbl

let ht_size =
  match Sys.getenv_opt "MEZOS_ANALYTICS_HT_SIZE" with
  | None -> 1000000
  | Some x ->
    try int_of_string x
    with _ -> Stdlib.failwith "Invalid MEZOS_ANALYTICS_HT_SIZE value"

let time_unit () =
  (Unix.time () |> Unix.gmtime).Unix.tm_yday

let key : _ -> int =
  let seed = match Sys.getenv_opt "MEZOS_ANALYTICS_SEED" with
    | None -> 537
    | Some x ->
      try int_of_string x
      with _ -> Stdlib.failwith "Invalid MEZOS_ANALYTICS_SEED value"
  in
  Hashtbl.seeded_hash seed

let h1 : (int, int) Hashtbl.t = Hashtbl.create ht_size
let h2 : (int, int) Hashtbl.t = Hashtbl.create ht_size
let current_time_unit = ref (time_unit ())
let past_time_unit = ref (!current_time_unit - 1)
let current_ht_ref : [ `h1 | `h2 ] ref = ref `h1

let pkh k =
  let open Tezos_protocol_016_PtMumbai.Protocol.Alpha_context.Contract in
  match of_b58check k with
  | Error _ -> None
  | Ok (Implicit k) -> Some k
  | Ok (Originated _) -> None

let add ks =
  (* FIXME: avoid computing [d] for each request *)
  let d = time_unit () in
  List.iter
    (fun (ua, k) ->
       match pkh k with
       | None -> ()
       | Some k ->
         let ht =
           if d = !current_time_unit then
             match !current_ht_ref with
             | `h1 -> h1
             | `h2 -> h2
           else
             let () = past_time_unit := !current_time_unit in
             let () = current_time_unit := d in
             match !current_ht_ref with
             | `h1 ->
               Hashtbl.reset h2;
               current_ht_ref := `h2;
               h2
             | `h2 ->
               Hashtbl.reset h1;
               current_ht_ref := `h1;
               h1
         in
         let k = key (ua, k) in
         match Hashtbl.find_opt ht k with
         | Some v -> Hashtbl.replace ht k (v + 1)
         | None -> if Hashtbl.length ht < ht_size then Hashtbl.add ht k 1)
    ks

let analytics_middleware =
  let headers_field =
    match Sys.getenv_opt "MEZOS_ANALYTICS_HEADERS_FIELD" with
    | None -> "user-agent"
    | Some x -> x
  in
  fun (server : Tezos_rpc_http_server.RPC_server.server) (conn : Cohttp_lwt_unix.Server.conn) (req : Cohttp.Request.t) (body : Cohttp_lwt.Body.t) ->
    let () =
      let open Cohttp in
      let uri = Uri.of_string (Request.resource req) in
      match Uri.path uri with
      | "/balances" ->
        let ua = Header.get (Request.headers req) headers_field in
        let accounts =
          List.fold_left
            (fun acc -> function ("pkh", [pkh]) -> (ua, pkh) :: acc | _ -> acc)
            []
            (Uri.query uri)
        in
        add accounts
      | _ -> ()
    in
    Tezos_rpc_http_server.RPC_server.resto_callback server conn req body
type daily_metrics =
  { time_unit: int
  ; hits: (int * int) array
  }

type full_metrics =
  { past: daily_metrics
  ; current: daily_metrics
  }

let json_encoding =
  let open Data_encoding in
  let int64 = conv Int64.of_int (fun _ -> invalid_arg "Mezos_analytics.json_encoding.int64") int64 in
  let value = tup2 int64 int64 in
  let hits = array value in
  let daily_metrics = obj2 (req "time_unit" int64) (req "hits" hits) in
  let full_metrics = obj2 (req "past" daily_metrics) (req "current" daily_metrics) in
  conv
    (fun m -> ((m.past.time_unit, m.past.hits), (m.current.time_unit, m.current.hits)))
    (fun _ -> invalid_arg "Mezos_analytics.json_encoding")
    full_metrics

(* FIXME: avoid intermediate array *)
let read ht =
  let len = Hashtbl.length ht in
  let arr = Array.make len (0, 0) in
  let i = ref 0 in
  Hashtbl.iter (fun k v ->
      Array.set arr !i (k, v);
      incr i
    ) ht ;
  arr

let read_past () =
  read @@ match !current_ht_ref with `h1 -> h2 | `h2 -> h1

let read_both () =
  let past, current = match !current_ht_ref with `h1 -> h2, h1 | `h2 -> h1, h2 in
  let past = read past in
  let current = read current in
  let past_time_unit = !past_time_unit in
  let current_time_unit = !current_time_unit in
  { past = { time_unit = past_time_unit ; hits = past }
  ; current = { time_unit = current_time_unit ; hits = current }
  }
