(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Caqti_type.Std
open Caqti_request
open Tezos_sql

open Tezos_protocol_014_PtKathma.Protocol
open Protocol
open Alpha_context
open Protocol_client_context

(**# let _ =
   if proto >= "014_" then
     begin
       print_string "let contract_unwrap = function Contract.Originated ch -> ch | _ -> assert false\n";
     end
   else
       print_string "let contract_unwrap x = x\n"
#**)

(**# let _ =
   if proto >= "015_" then print_string "module Script_ir_translator = Script_ir_unparser" #**)



module FA2 = struct

  let balance cctxt (token_kt:Contract.t) tokenid accounts =
    let open Lwt_result_syntax in
    let open Mezos_fa2 in
    let* r =
      run_balance_view_offchain
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ~contract:token_kt
        ~requests:(List.map (fun a -> a, tokenid) accounts)
        ~unparsing_mode:Script_ir_translator.Readable ()
    in
    let r =
      List.map (fun (c, b) -> c, Mezos_services_common.Accounts.Z b) r
    in
    return r

end

module FA12 = struct

  let balance cctxt (token_kt:Contract.t) accounts =
    List.map_es (fun addr ->
        let open Lwt_result_syntax in
        let* expr =
          let open Client_proto_fa12 in
          run_view_action
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ~contract:(contract_unwrap token_kt)
            ~action:(Get_balance (addr, (token_kt, None)))
            ~unparsing_mode:Script_ir_translator.Readable ()
        in
        let res =
          let open Tezos_micheline.Micheline in
          match root expr with
          | Int (_, z) -> z
          | _ -> Z.zero
        in
        return (addr, Mezos_services_common.Accounts.Z res)
      ) accounts

end

module Tez = struct

  let balance cctxt accounts =
    List.map_es (fun addr ->
        let open Lwt_result_syntax in
        Lwt.bind
          (Alpha_services.Contract.balance cctxt (cctxt#chain, cctxt#block) addr)
          (function
            | Ok balance ->
              return (addr, Mezos_services_common.Accounts.Tez balance)
            | Error _ ->
              return (addr, Mezos_services_common.Accounts.Tez (Tez.zero))
          )
      ) accounts
end

let make cctxt token_kt tokenid accounts =
  match token_kt, tokenid with
  | Some token_kt, None ->
    FA12.balance cctxt token_kt accounts
  | None, _ ->
    Tez.balance cctxt accounts
  | Some token_kt, Some tokenid ->
    FA2.balance cctxt token_kt tokenid accounts
